module Main where

readInt :: String -> Int
readInt = read

addIfSup :: (Int, Int) -> Int -> (Int, Int)
addIfSup (cpt, prev) curr
  | curr > prev = (cpt + 1, curr)
  | otherwise = (cpt, curr)

contentToList :: String -> [Int]
contentToList = map readInt . words

countIncreases :: [Int] -> Int
countIncreases lst = fst $ foldl addIfSup (-1, 0) lst

computeList :: [Int] -> [Int]
computeList (x1:x2:x3:xs) = (x1 + x2 + x3) : computeList (x2:x3:xs)
computeList _ = []

main :: IO ()
main = do
    content <- readFile "../../inputs/day_1.input"
    print $ countIncreases $ computeList $ contentToList content

