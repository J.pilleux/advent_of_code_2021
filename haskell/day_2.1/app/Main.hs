module Main where

readInt :: String -> Int
readInt = read

extractNumber :: String -> Int
extractNumber str = readInt $ last $ words str

updatePosition :: (Int, Int, Int) -> String -> (Int, Int, Int)
updatePosition (x, y, aim) ('f':str) = (x', y', aim)
  where
    ext = extractNumber str
    x' = x + ext
    y' = y + aim * ext
updatePosition (x, y, aim) ('d':str) = (x, y, aim')
  where
    aim' = aim + extractNumber str
updatePosition (x, y, aim) ('u':str) = (x, y, aim')
  where
    aim' = aim - extractNumber str
updatePosition t _ = t

addTuple :: (Int, Int, Int) -> Int
addTuple (x, y, _) = x * y

main :: IO ()
main = do
  content <- fmap lines (readFile "../../inputs/day_2.input")
  print $ addTuple $ foldl updatePosition (0, 0, 0) content
