module Main where

readInt :: String -> Int
readInt = read

extractNumber :: String -> Int
extractNumber str = readInt $ last $ words str

updatePosition :: (Int, Int) -> String -> (Int, Int)
updatePosition (x,y) ('f':str) = (x + extractNumber str, y)
updatePosition (x,y) ('d':str) = (x, y + extractNumber str)
updatePosition (x,y) ('u':str) = (x, y - extractNumber str)
updatePosition t _ = t

addTuple :: (Int, Int) -> Int
addTuple (x, y) = x * y

main :: IO ()
main = do
    content <- fmap lines (readFile "../../inputs/day_2.input")
    print $ addTuple $ foldl updatePosition (0, 0) content
