module Main where

import Data.Char (digitToInt)

addList :: [Int] -> String -> [Int]
addList [] [] = []
addList [x] [y]
    | y == '1' = [x + 1]
    | otherwise = [x]
addList (x:xs) (y:ys)
    | y == '1' = x + 1 : addList xs ys
    | otherwise = x : addList xs ys
addList _ _ = []

countGammaEpsilon :: Int -> [Int] -> String
countGammaEpsilon _ [] = []
countGammaEpsilon middle (x:xs)
    | x > middle = '1' : countGammaEpsilon middle xs
    | otherwise = '0' : countGammaEpsilon middle xs

getBinaryArr :: [String] -> String
getBinaryArr content = array
  where
    array = countGammaEpsilon middle table
    middle = length content `div` 2
    table = foldl addList empty content
    empty = [0 | _ <- [0 .. length content]]

reverseList :: [a] -> [a]
reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

convert :: String -> Int
convert [] = 0
convert (x:xs) = (digitToInt x) + 2 * convert xs

computeResult :: String -> Int
computeResult array = result
  where
    result = gamma * epsilon
    gamma = convert $ reverseList $ array
    epsilon = convert $ oposite $ reverseList $ array

oposite :: String -> String
oposite [] = []
oposite (x:xs)
    | x == '1' = '0' : oposite xs
    | otherwise = '1' : oposite xs

main = do
    content <- fmap lines (readFile "../../inputs/day_3.input")
    print $ computeResult $ getBinaryArr content
