module Main where

readInt :: String -> Int
readInt = read

addIfSup :: (Int, Int) -> Int -> (Int, Int)
addIfSup (cpt, prev) curr
  | curr > prev = (cpt + 1, curr)
  | otherwise = (cpt, curr)

contentToList :: String -> [Int]
contentToList = map readInt . words

countDecreases :: [Int] -> Int
countDecreases lst = fst $ foldl addIfSup (-1, 0) lst

main :: IO ()
main = do
    content <- readFile "../../inputs/day_1.input"
    print $ countDecreases $ contentToList content

