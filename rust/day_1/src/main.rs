use std::fs::File;
use std::io::{BufRead, BufReader, Result};

fn main() -> Result<()> {
    let mut incs: i32 = 0;
    let mut prev: i32 = 0;
    let file = File::open("../../inputs/day_1.input")?;
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let l: i32 = line?.parse().unwrap();
        if prev == 0 {
            prev = l;
            continue;
        }
        if l > prev {
            incs += 1;
        }
        prev = l;
    }
    println!("{}", incs);
    Ok(())
}

