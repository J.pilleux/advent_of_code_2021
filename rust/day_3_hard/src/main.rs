use std::fs::File;
use std::io::Read;


fn count_bit(arr: &Vec<String>, number: usize, to_count: char) -> char {
    let mut cpt_1: usize = 0;
    for s in arr {
        if check_char(s, number, to_count) {
            cpt_1 += 1;
        }
    }
    let cpt_0 = arr.len() - cpt_1;
    let c = if to_count == '1' {
        if cpt_1 >= cpt_0 {
            '1'
        } else {
            '0'
        }
    } else {
        if cpt_1 > cpt_0 {
            '1'
        } else {
            '0'
        }
    };
    return c;
}

fn check_char(s: &str, num: usize, c: char) -> bool {
    let str_c: char = s.chars().nth(num).unwrap();
    if str_c == c {
        return true;
    }
    return false;
}

fn get_gamma(array: Vec<String>, to_count: char) -> String {
    let mut input = array;
    let mut current_pos: usize = 0;
    while input.len() > 1 {
        let most_present_digit: char = count_bit(&input, current_pos, to_count);
        input = input
            .iter()
            .filter(|n| check_char(&n, current_pos, most_present_digit))
            .cloned()
            .collect();
        current_pos += 1;
    }
    String::from(&input[0])
}

fn binary_to_u32(s: String) -> u32 {
    let mut binary_digit = s.chars().count();
    let mut real_num: u32 = 0;
    for c in s.chars() {
        let mut temp_var = 2u32.pow(binary_digit.try_into().unwrap());
        temp_var /= 2;
        if c == '1' {
            real_num += temp_var;
        }
        binary_digit -= 1;
    }
    return real_num;
}

fn file_to_string() -> Vec<String> {
    let mut file = File::open("../../inputs/day_3.input").unwrap();
    let mut buf: String = String::new();
    file.read_to_string(&mut buf).unwrap();
    return buf.split('\n').map(String::from).filter(|line| line != "").collect();
}

fn main() {
    let input = file_to_string();
    let gamma = get_gamma(input.to_vec(), '1');
    let epsilon = get_gamma(input.to_vec(), '0');
    let ratio = binary_to_u32(gamma.to_string()) * binary_to_u32(epsilon.to_string());
    println!("{}", ratio)
}
