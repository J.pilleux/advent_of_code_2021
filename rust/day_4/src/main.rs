use std::{fs::File, io::Read};

fn file_to_string() -> Vec<String> {
    let mut file = File::open("../../inputs/day_4.input").unwrap();
    let mut buf: String = String::new();
    file.read_to_string(&mut buf).unwrap();
    buf.split('\n').map(String::from).filter(|line| line != "").collect()
}

fn input_to_data(input: Vec<String>) -> (String, Vec<String>) {
    let sequence = &input[0];

    (sequence.to_string(), Vec::new())
}

fn main() {
    let mut input: Vec<String> = file_to_string();
    let (sequence, grids) = input_to_data(input);
    println!("{}", sequence);
}
